Function: gcharalgebraic
Section: number_fields
C-Name: gcharalgebraic
Prototype: GDG
Help: gcharalgebraic(gc,{type}): returns a matrix whose columns form a basis of
 the algebraic Grossencharacters in gc. If type is specified as a vector of
 integer exponents [p,q] for each complex place, return a vector containing
 an algebraic character of that infinity type (or the empty vector if such a
 character does not exist).
Doc: \var{gc} being the structure returned by \kbd{gcharinit}, return a \typ{MAT}
 whose columns form a basis of the subgroup of algebraic Grossencharacters in
 \var{gc} (Weil type A0). The last component is interpreted as a power of the
 norm.

 If \var{type} is a \typ{VEC} of length $\var{gc}\kbd{.r1}+\var{gc}\kbd{.r2}$,
 containing a pair of integers $[p_\tau,q_\tau]$ for each complex
 embedding~$\tau$, return a \typ{VEC} containing a character whose infinity type
 at~$\tau$ is
 $$
  z \mapsto z^{-p_\tau}\bar{z}^{-q_\tau}
 $$
 if such a character exists, or empty otherwise.
 The full set of characters of that infinity type is obtained by multiplying by
 the group of finite order characters.

 \bprog
 ? bnf = bnfinit(x^4-2*x^3+23*x^2-22*x+6,1);
 ? gc = gcharinit(bnf,1);
 ? gc.cyc
 % = [6, 0, 0, 0, 0.E-57]
 ? gcharalgebraic(gc)
 % =
 [1    0    0  0]
 [0    1    0  0]
 [0    0    1  0]
 [0    0    0  0]
 [0 -1/2 -1/2 -1]
 ? gcharalgebraic(gc,[[1,1],[0,1]])
 % = [] \\ @com $p_\tau+q_\tau$ must be constant for an algebraic character to exist
 ? gcharalgebraic(gc,[[1,1],[0,2]])
 % = [[0, 1, 1, 0, -1]~]
 @eprog
